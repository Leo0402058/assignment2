/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.crudmethods.databean;

import static com.oracle.jrockit.jfr.ContentType.Timestamp;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 *
 * @author User
 */
public class Patient {
    private int PATIENTID;
    private String LASTNAME;
    private String FIRSTNAME;
    private String DIAGNOSIS;
    private Timestamp ADMISSIONDATE;
    private Timestamp RELEASEDATE;
     // Here is the join
    private List<Inpatient> listOfInpatient;
    private List<Surgical> listOfSurgical;
    private List<Medication> listOfMediacation;
    
    public Patient() {
        this("","","", new Timestamp(Instant.now().toEpochMilli()),new Timestamp(Instant.now().toEpochMilli()));
    }

    public Patient(String lastname,String firstname, String diagnosis, Timestamp admissiondate, Timestamp releasedate) {
        super();
        
        this.LASTNAME = lastname;
        this.FIRSTNAME = firstname;
        this.DIAGNOSIS = diagnosis;
        this.ADMISSIONDATE = admissiondate;
        this.RELEASEDATE = releasedate;
    }


    public List<Surgical> getListOfSurgical() {
        return listOfSurgical;
    }

    public void setListOfSurgical(List<Surgical> listOfSurgical) {
        this.listOfSurgical = listOfSurgical;
    }

    public List<Medication> getListOfMediacation() {
        return listOfMediacation;
    }

    public void setListOfMediacation(List<Medication> listOfMediacation) {
        this.listOfMediacation = listOfMediacation;
    }
    
    

    public List<Inpatient> getListOfInpatient() {
        return listOfInpatient;
    }

    public void setListOfInpatient(List<Inpatient> listOfInpatient) {
        this.listOfInpatient = listOfInpatient;
    }
   
    
    
    
    public Timestamp getRELEASEDATE() {
        return RELEASEDATE;
    }

    public void setRELEASEDATE(Timestamp RELEASEDATE) {
        this.RELEASEDATE = RELEASEDATE;
    }

    public int getPATIENTID() {
        return PATIENTID;
    }

    public void setPATIENTID(int PATIENTID) {
        this.PATIENTID = PATIENTID;
    }

    public String getLASTNAME() {
        return LASTNAME;
    }

    public void setLASTNAME(String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }

    public String getFIRSTNAME() {
        return FIRSTNAME;
    }

    public void setFIRSTNAME(String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }

    public String getDIAGNOSIS() {
        return DIAGNOSIS;
    }

    public void setDIAGNOSIS(String DIAGNOSIS) {
        this.DIAGNOSIS = DIAGNOSIS;
    }

    public Timestamp getADMISSIONDATE() {
        return ADMISSIONDATE;
    }

    public void setADMISSIONDATE(Timestamp ADMISSIONDATE) {
        this.ADMISSIONDATE = ADMISSIONDATE;
    }

    @Override
    public String toString() {
        return "Patient{" + "PATIENTID=" + PATIENTID + ", LASTNAME=" + LASTNAME + ", FIRSTNAME=" + FIRSTNAME + ", DIAGNOSIS=" + DIAGNOSIS + ", ADMISSIONDATE=" + ADMISSIONDATE + '}'+",RELEASEDATE=" + RELEASEDATE;
    }
   
}