/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.crudmethods.databean;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 *
 * @author User
 */
public class Surgical {

    private int ID;
    private int PATIENTID;
    private Timestamp DATEOFSURGERY;
    private String SURGERY;
    private double ROOMFEE;
    private double SURGEONFEE;
    private double SUPPLIES;
    
    public Surgical() {
        this(-1, new Timestamp(Instant.now().toEpochMilli()), "",0.0,0.0,0.0);
    }

       public  Surgical(int patientid, Timestamp dateofsurgery,String surgery, double roomfee, double surgeonfee,double supples) {
        
        this.PATIENTID = patientid;
        this.DATEOFSURGERY = dateofsurgery;
        this.SURGERY = surgery;
        this.ROOMFEE = roomfee;
        this.SURGEONFEE = surgeonfee;
        this.SUPPLIES = supples;
       
    }
    
    

    public List<Medication> getListOfMedication() {
        return listOfMedication;
    }

    // Here is the join
    public void setListOfMedication(List<Medication> listOfMedication) {
        this.listOfMedication = listOfMedication;
    }
    private List<Medication> listOfMedication;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPATIENTID() {
        return PATIENTID;
    }

    public void setPATIENTID(int PATIENTID) {
        this.PATIENTID = PATIENTID;
    }

    public Timestamp getDATEOFSURGERY() {
        return DATEOFSURGERY;
    }

    public void setDATEOFSURGERY(Timestamp DATEOFSURGERY) {
        this.DATEOFSURGERY = DATEOFSURGERY;
    }

    public String getSURGERY() {
        return SURGERY;
    }

    public void setSURGERY(String SURGERY) {
        this.SURGERY = SURGERY;
    }

    public double getROOMFEE() {
        return ROOMFEE;
    }

    public void setROOMFEE(double ROOMFEE) {
        this.ROOMFEE = ROOMFEE;
    }

    public double getSURGEONFEE() {
        return SURGEONFEE;
    }

    public void setSURGEONFEE(double SURGEONFEE) {
        this.SURGEONFEE = SURGEONFEE;
    }

    public double getSUPPLIES() {
        return SUPPLIES;
    }

    public void setSUPPLIES(double SUPPLIES) {
        this.SUPPLIES = SUPPLIES;
    }

    @Override
    public String toString() {
        return "Surgical{" + "ID=" + ID + ", PATIENTID=" + PATIENTID + ", DATEOFSURGERY=" + DATEOFSURGERY + ", SURGERY=" + SURGERY + ", ROOMFEE=" + ROOMFEE + ", SURGEONFEE=" + SURGEONFEE + ", SUPPLIES=" + SUPPLIES + '}';
    }

}
