/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.crudmethods.persistence;

import com.pengxing.crudmethods.databean.Inpatient;
import com.pengxing.crudmethods.databean.Medication;
import com.pengxing.crudmethods.databean.Patient;
import com.pengxing.crudmethods.databean.Surgical;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author User
 */
public interface HospitalDAO {
     // Create
    public Patient createPatient(Patient patient) throws SQLException;

    public Inpatient createInpatient(Inpatient inpatient) throws SQLException;
    
    public int createMedication(Medication medication) throws SQLException;

    public int createSurgical(Surgical surgical) throws SQLException;

    // Read
    public List<Patient> findAllPatient() throws SQLException;
    
    public Inpatient findInpatientByID(int id) throws SQLException;
    
    public Surgical findSurgicalByID(int id) throws SQLException;
    
    public Medication findMedicationByID(int id) throws SQLException;
    
//    public List<Inpatient> findAllInpatient(Inpatient inpatient) throws SQLException;
//    
//    public List<Surgical> findAllSurgical(Patient patient) throws SQLException ;
//    
//    public List<Medication> findAllMedication(Patient patient) throws SQLException;
    
    public Patient findPatientById(int id) throws SQLException;
    
 //   public Inpatient findIdInpatient(Patient patient) throws SQLException;
    
  //  public Surgical findIdSurgical(Patient patient) throws SQLException;
    
 //   public Medication findIdMedication(Patient patient) throws SQLException ;
    
    public Patient findLastName(String name) throws SQLException;
    
    public Inpatient findLastNameInpatient(String name) throws SQLException;
    
    public Surgical findLastNameSugical(String name) throws SQLException;
    
    public Medication findLastNameMedication(String name) throws SQLException;
    
    // Update

    public void updatePatient(Patient patient) throws SQLException;

    public void updateInpatient(Inpatient inpatient) throws SQLException;
    
    public void updateMedication(Medication medication) throws SQLException;

    public void updateSurgical(Surgical surgical) throws SQLException;

    // Delete 
    public int deletePatient(Patient patient) throws SQLException;

    public int deleteInpatient(Inpatient inpatient) throws SQLException;
    
    public int deleteMedication(Medication medication) throws SQLException;

    public int deleteSurgical(Surgical surgical) throws SQLException;

    
}
