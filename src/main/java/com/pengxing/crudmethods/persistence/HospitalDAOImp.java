/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.crudmethods.persistence;

import com.pengxing.crudmethods.databean.Inpatient;
import com.pengxing.crudmethods.databean.Medication;
import com.pengxing.crudmethods.databean.Patient;
import com.pengxing.crudmethods.databean.Surgical;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author User
 */
public class HospitalDAOImp implements HospitalDAO {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "pengxing";
    private final String password = "concordia";

    /**
     *
     * Create Part
     *
     */
    // create patient record
    @Override
    public Patient createPatient(Patient patient) throws SQLException {

        String sql = "INSERT INTO PATIENT (LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(sql);) {
            
            pStatement.setString(1, patient.getLASTNAME());
            pStatement.setString(2, patient.getFIRSTNAME());
            pStatement.setString(3, patient.getDIAGNOSIS());
            pStatement.setTimestamp(4, patient.getADMISSIONDATE());
            pStatement.setTimestamp(5, patient.getRELEASEDATE());

            pStatement.executeUpdate();
        }
        return patient;
    }

    //create inpatient record
    @Override
    public Inpatient createInpatient(Inpatient inpatient) throws SQLException {
        
        String sql = "INSERT INTO INPATIENT(PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES) values (?, ?, ?, ?, ?,?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(sql);) {
            pStatement.setInt(1, inpatient.getPATIENTID());
            pStatement.setTimestamp(2, inpatient.getDateofstay());
            pStatement.setString(3, inpatient.getROOMNUMBER());
            pStatement.setDouble(4, inpatient.getDAILYRATE());
            pStatement.setDouble(5, inpatient.getSUPPLIES());
            pStatement.setDouble(6, inpatient.getSERVICES());

            pStatement.executeUpdate();
        }
        return inpatient;
    }

    // Create medication record
    @Override
    public int createMedication(Medication medication) throws SQLException {
        int records = 0;
        String sql = "INSERT INTO MEDICATION(PATIENTID,DATEOFMED,MED,UNITCOST,UNITS) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(sql);) {

            pStatement.setInt(1, medication.getPATIENTID());
            pStatement.setTimestamp(2, medication.getDATEOFMED());
            pStatement.setString(3, medication.getMED());
            pStatement.setDouble(4, medication.getUNITCOST());
            pStatement.setDouble(5, medication.getUNITS());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    // create surgical record
    @Override
    public int createSurgical(Surgical surgical) throws SQLException {
        int records = 0;
        String sql = "INSERT INTO SURGICAL (PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES) VALUES (?, ?, ?, ?, ?, ?)";//WHERE PAITENTID IN (SELECT PATIENTID FROM PATIENT)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(sql);) {
            pStatement.setInt(1, surgical.getPATIENTID());
            pStatement.setTimestamp(2, surgical.getDATEOFSURGERY());
            pStatement.setString(3, surgical.getSURGERY());
            pStatement.setDouble(4, surgical.getROOMFEE());
            pStatement.setDouble(5, surgical.getSURGEONFEE());
            pStatement.setDouble(6, surgical.getSUPPLIES());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    /**
     * Update Method Part
     *
     */
    // patient update part
    @Override
    public void updatePatient(Patient patient) throws SQLException {

        String updateQuery = "UPDATE PATIENT SET LASTNAME = ?, FIRSTNAME = ?, DIAGNOSIS = ?, ADMISSIONDATE = ?, RELEASEDATE = ? WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, patient.getLASTNAME());
            ps.setString(2, patient.getFIRSTNAME());
            ps.setString(3, patient.getDIAGNOSIS());
            ps.setTimestamp(4, patient.getADMISSIONDATE());
            ps.setTimestamp(5, patient.getRELEASEDATE());
            ps.setInt(6, patient.getPATIENTID());

            ps.executeUpdate();
        }

    }

    @Override
    public void updateInpatient(Inpatient inpatient) throws SQLException {

        String updateQuery = "UPDATE INPATIENT SET PATIENTID = ?, DATEOFSTAY = ?, ROOMNUMBER = ?, DAILYRATE = ?, SUPPLIES = ?, SERVICES = ? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setInt(1, inpatient.getPATIENTID());
            ps.setTimestamp(2, inpatient.getDateofstay());
            ps.setString(3, inpatient.getROOMNUMBER());
            ps.setDouble(4, inpatient.getDAILYRATE());
            ps.setDouble(5, inpatient.getSUPPLIES());
            ps.setDouble(6, inpatient.getSERVICES());
            ps.setInt(7, inpatient.getID());

            ps.executeUpdate();
        }

    }

    @Override
    public void updateMedication(Medication medication) throws SQLException {

        String updateQuery = "UPDATE MEDICATION SET PATIENTID = ?, DATEOFMED = ?, MED = ?, UNITCOST = ?, UNITS = ? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setInt(1, medication.getPATIENTID());
            ps.setTimestamp(2, medication.getDATEOFMED());
            ps.setString(3, medication.getMED());
            ps.setDouble(4, medication.getUNITCOST());
            ps.setDouble(5, medication.getUNITS());
            ps.setInt(6, medication.getID());
            
            ps.executeUpdate();
        }

    }

    @Override
    public void updateSurgical(Surgical surgical) throws SQLException {

        String updateQuery = "UPDATE SURGICAL SET PATIENTID = ?, DATEOFSURGERY = ?, SURGERY = ?, ROOMFEE = ?, SURGEONFEE = ?, SUPPLIES = ? WHERE ID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setInt(1, surgical.getPATIENTID());
            ps.setTimestamp(2, surgical.getDATEOFSURGERY());
            ps.setString(3, surgical.getSURGERY());
            ps.setDouble(4, surgical.getROOMFEE());
            ps.setDouble(5, surgical.getSURGEONFEE());
            ps.setDouble(6, surgical.getSUPPLIES());
            ps.setInt(7, surgical.getID());
            
            ps.executeUpdate();
        }

    }

    /**
     *
     * Record Delete Part
     *
     * @throws SQLException
     */
    // Delete a patient record with all related detail records deleted as well
    @Override
    public int deletePatient(Patient patient) throws SQLException {
        int result;
        
        deleteAllInpatient(patient);
        deleteAllSurgical(patient);
        deleteAllMedication(patient);
        
        String deleteQuery = "DELETE FROM PATIENT WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPATIENTID());
            result = ps.executeUpdate();
        }

        log.info("# of records deleted : " + result);
        return result;
    }

    private int deleteAllInpatient(Patient patient) throws SQLException {
        int result;
        String deleteQuery = "DELETE FROM INPATIENT WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPATIENTID());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    private int deleteAllSurgical(Patient patient) throws SQLException {
        int result;
        String deleteQuery = "DELETE FROM SURGICAL WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPATIENTID());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    private int deleteAllMedication(Patient patient) throws SQLException {
        int result;
        String deleteQuery = "DELETE FROM MEDICATION WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPATIENTID());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public int deleteInpatient(Inpatient inpatient) throws SQLException {
        int result;
        String deleteQuery = "DELETE FROM INPATIENT WHERE ID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, inpatient.getID());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public int deleteMedication(Medication medication) throws SQLException {
        int result;
        String deleteQuery = "DELETE FROM MEDICATION WHERE ID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, medication.getID());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public int deleteSurgical(Surgical surgical) throws SQLException {
        int result;
        String deleteQuery = "DELETE FROM SURGICAL WHERE ID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, surgical.getID());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    // create an object of patient type from the current record in the result set
    private Patient createPatientdata(ResultSet resultSet) throws SQLException {
        Patient patient = new Patient();
        patient.setPATIENTID(resultSet.getInt("PATIENTID"));
        patient.setLASTNAME(resultSet.getString("LASTNAME"));
        patient.setFIRSTNAME(resultSet.getString("FIRSTNAME"));
        patient.setDIAGNOSIS(resultSet.getString("DIAGNOSIS"));
        patient.setADMISSIONDATE(resultSet.getTimestamp("ADMISSIONDATE"));
        patient.setRELEASEDATE(resultSet.getTimestamp("RELEASEDATE"));
        return patient;
    }

    // create an object of inpatient type from the current record in the result set
    private Inpatient createInpatientdata(ResultSet resultSet) throws SQLException {
        Inpatient inpatient = new Inpatient();
        inpatient.setPATIENTID(resultSet.getInt("PATIENTID"));
        inpatient.setDateofstay(resultSet.getTimestamp("DATEOFSTAY"));
        inpatient.setROOMNUMBER(resultSet.getString("ROOMNUMBER"));
        inpatient.setDAILYRATE(resultSet.getDouble("DAILYRATE"));
        inpatient.setSUPPLIES(resultSet.getDouble("SUPPLIES"));
        inpatient.setSERVICES(resultSet.getDouble("SERVICES"));
        return inpatient;
    }

    // create an object of Surgical type from the current record in the result set
    private Surgical createSurgicaldata(ResultSet resultSet) throws SQLException {
        Surgical surgical = new Surgical();
        surgical.setPATIENTID(resultSet.getInt("PATIENTID"));
        surgical.setDATEOFSURGERY(resultSet.getTimestamp("DATEOFSURGERY"));
        surgical.setSURGERY(resultSet.getString("SURGERY"));
        surgical.setROOMFEE(resultSet.getDouble("ROOMFEE"));
        surgical.setSURGEONFEE(resultSet.getDouble("SURGEONFEE"));
        surgical.setSUPPLIES(resultSet.getDouble("SUPPLIES"));
        return surgical;
    }

    // create an object of Surgical type from the current record in the result set
    private Medication createMedicationdata(ResultSet resultSet) throws SQLException {
        Medication medication = new Medication();
        medication.setPATIENTID(resultSet.getInt("PATIENTID"));
        medication.setDATEOFMED(resultSet.getTimestamp("DATEOFMED"));
        medication.setMED(resultSet.getString("MED"));
        medication.setUNITCOST(resultSet.getDouble("UNITCOST"));
        medication.setUNITS(resultSet.getDouble("UNITS"));
        return medication;
    }

//
    /**
     *
     * Find Record Part
     *
     *
     */
    // find a patient record by its patient id
    @Override
    public Patient findPatientById(int id) throws SQLException {
        Patient patient = new Patient();
        String selectQuery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, id);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    patient = createPatientdata(resultSet);
                }
            }
        }
        allInpatientByID(patient);
        allMedicationByID(patient);
        allSurgicalByID(patient);
        return patient;
    }

    public Inpatient allInpatientByID(Patient patient) throws SQLException {
        Inpatient inpatient = new Inpatient();
        String selectQuery = "SELECT PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, patient.getPATIENTID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    inpatient = createInpatientdata(resultSet);

                }
            }
        }

        return inpatient;
    }

    public Medication allMedicationByID(Patient patient) throws SQLException {
        Medication medication = new Medication();
        String selectQuery = "SELECT PATIENTID, DATEOFMED, MED, UNITCOST,UNITS FROM MEDICATION WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, patient.getPATIENTID()
            );
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {

                    medication = createMedicationdata(resultSet);
                }
            }
        }

        return medication;

    }

    public Surgical allSurgicalByID(Patient patient) throws SQLException 
   {
         Surgical surgical = new Surgical();
        String selectQuery = "SELECT PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, patient.getPATIENTID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                   surgical = createSurgicaldata(resultSet);
            }
        }
        return surgical;
        
    }
   }
    
    // Find an inpatient record by its ID
    @Override
    public Inpatient findInpatientByID(int id) throws SQLException {
        Inpatient inpatient = new Inpatient();

        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE ID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    inpatient = createInpatientdata(resultSet);

                }

            }
        }
        return inpatient;
    }

    // Find a medication record by a medication ID
    @Override
    public Medication findMedicationByID(int id) throws SQLException {
        Medication medication = new Medication();
        String selectQuery = "SELECT ID,PATIENTID, DATEOFMED, MED, UNITCOST,UNITS FROM MEDICATION WHERE ID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {

                    medication = createMedicationdata(resultSet);
                }
            }
        }
        return medication;
    }

    // Find a surgical record by a surgical ID
    @Override
    public Surgical findSurgicalByID(int id) throws SQLException {
        Surgical surgical = new Surgical();

        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE ID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                   surgical = createSurgicaldata(resultSet);
            }
        }
        return surgical;
      }
    }

    @Override
    public List<Patient> findAllPatient() throws SQLException {
        List<Patient> rows = new ArrayList<>();
        Patient patient = new Patient();
        String selectQuery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                rows.add(createPatientdata(resultSet));
            }
        }
        return rows;
    }
    
    public List<Inpatient> findAllInpatient() throws SQLException
     {
        List<Inpatient> rows = new ArrayList<>();
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            try (ResultSet resultSet = pStatement.executeQuery()) {

                while (resultSet.next()) {

                    rows.add(createInpatientdata(resultSet));
                }

            }
        }

        return rows;
    }

    
    public List<Surgical> findAllSurgical() throws SQLException {
        List<Surgical> rows = new ArrayList<>();
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {

                    rows.add(createSurgicaldata(resultSet));
                }
            }
        }

        return rows;
    }

    public List<Medication> findAllMedication() throws SQLException {
        List<Medication> rows = new ArrayList<>();
        String selectQuery = "SELECT ID,PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {

                    rows.add(createMedicationdata(resultSet));
                }
            }
        }

        return rows;
    }

    @Override
    public Patient findLastName(String name) throws SQLException {
        Patient patient = new Patient();
        String selectQuery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE LASTNAME = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, name);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {

                    patient = createPatientdata(resultSet);

                }
            }

        }
        findLastNameInpatient(name);
        findLastNameSugical(name);
        findLastNameMedication(name);
        return patient;
    }

    @Override
    public Inpatient findLastNameInpatient(String name) throws SQLException {
        Inpatient inpatient = new Inpatient();
        String selectQuery = "SELECT PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE PATIENTID = (SELECT PATIENTID FROM PATIENT WHERE LASTNAME = ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, name);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    inpatient = createInpatientdata(resultSet);
                }
            }
        }

        return inpatient;
    }

    @Override
    public Surgical findLastNameSugical(String name) throws SQLException {
        Surgical surgical = new Surgical();
        String selectQuery = "SELECT PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE PATIENTID = (SELECT PATIENTID FROM PATIENT WHERE LASTNAME = ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, name);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {

                    surgical = createSurgicaldata(resultSet);

                }
            }
        }

        return surgical;
    }

    @Override
    public Medication findLastNameMedication(String name) throws SQLException {
        Medication medication = new Medication();
        String selectQuery = "SELECT PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE PATIENTID = (SELECT PATIENTID FROM PATIENT WHERE LASTNAME = ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, name);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {

                    medication = createMedicationdata(resultSet);

                }
            }
        }
        return medication;
    }

}
